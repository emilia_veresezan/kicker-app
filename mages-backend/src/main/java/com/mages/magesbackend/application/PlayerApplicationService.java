package com.mages.magesbackend.application;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mages.magesbackend.domain.Player;
import com.mages.magesbackend.infrastructure.persistence.PlayerRepository;

@Service
public class PlayerApplicationService {

    @Autowired
    private PlayerRepository playerRepository;

    public Player savePlayer(Player player) {
        return playerRepository.save(player);
    }

    public Player updateUser(Player player) {
        return playerRepository.save(player);
    }

    public List<Player> getPlayers() {
        return playerRepository.findAll();
    }

    public Player getPlayer(Long id) {
        return playerRepository.findById(id);
    }

    public List<Player> getPlayersByIds(Set<Long> playerIds){
       return playerRepository.findByIdIn( playerIds);
    }
}
