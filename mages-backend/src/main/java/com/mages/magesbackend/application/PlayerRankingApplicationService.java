package com.mages.magesbackend.application;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import com.mages.magesbackend.domain.MatchResult;
import com.mages.magesbackend.domain.Player;
import com.mages.magesbackend.domain.PlayerRanking;
import com.mages.magesbackend.domain.PlayerResult;

@Service
public class PlayerRankingApplicationService {

    @Autowired
    PlayerApplicationService playerApplicationService;

    @Async
    public void updatePlayerRankings(List<MatchResult> matchResults) {
        matchResults.stream().forEach(mr -> {
            Player player = playerApplicationService.getPlayer(mr.getPlayerId());
            PlayerRanking playerRanking = player.getPlayerRanking();
            Integer newScore = playerRanking.getScore();

            if (mr.getPlayerResult() == PlayerResult.WIN) {
                newScore += 10;
            } else {
                newScore -= 10;
            }

            playerRanking.setScore(newScore);

            playerApplicationService.savePlayer(player);
        });

    }
}
