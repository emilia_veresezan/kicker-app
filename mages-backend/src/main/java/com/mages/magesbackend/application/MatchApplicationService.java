package com.mages.magesbackend.application;

import com.mages.magesbackend.domain.Match;
import com.mages.magesbackend.domain.MatchResult;
import com.mages.magesbackend.domain.Player;
import com.mages.magesbackend.domain.PlayerResult;
import com.mages.magesbackend.infrastructure.persistence.MatchRepository;
import com.mages.magesbackend.infrastructure.persistence.PlayerRepository;
import com.mages.magesbackend.infrastructure.persistence.ResultsRepository;
import com.mages.magesbackend.web.rest.dto.MatchRequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

<<<<<<< Updated upstream
import java.util.ArrayList;
=======
import java.util.Arrays;
>>>>>>> Stashed changes
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.transaction.Transactional;

@Service
public class MatchApplicationService {

    @Autowired
    MatchRepository matchRepository;

    @Autowired
    ResultsRepository resultsRepository;

    @Autowired
    PlayerRankingApplicationService playerRankingApplicationService;

    @Autowired
    PlayerApplicationService playerApplicationService;


    public Match saveMatch(Match match) {
        return matchRepository.save(match);
    }

    @Transactional
    public void submitResults(MatchRequestDto matchRequestDto) {
        Match match = matchRepository.save(new Match());

        List<MatchResult> matchResults = new ArrayList<>();
        MatchRequestDto.Team winningTeam = getWinnerTeam(matchRequestDto);
        MatchRequestDto.Team losingTeam = getLosingTeam(matchRequestDto);

        matchResults.addAll(winningTeam.getPlayerIds().stream().map(pid ->
            new MatchResult(pid, PlayerResult.WIN, winningTeam.getScore(), match.getId())).collect(Collectors.toList()));

        matchResults.addAll(losingTeam.getPlayerIds().stream().map(pid ->
            new MatchResult(pid, PlayerResult.LOSS, losingTeam.getScore(), match.getId())).collect(Collectors.toList()));

        playerRankingApplicationService.updatePlayerRankings(matchResults);

        resultsRepository.save(matchResults);
    }

    public List<MatchResult> getMatches(Long playerId) {
        return resultsRepository.findByPlayerId(playerId);
    }

<<<<<<< Updated upstream
    private MatchRequestDto.Team getWinnerTeam(MatchRequestDto matchRequestDto) {
        Integer team1Score = matchRequestDto.getTeam1().getScore();
        Integer team2Score = matchRequestDto.getTeam2().getScore();
        if (team1Score.compareTo(team2Score) > 0) {
            return matchRequestDto.getTeam1();
        } else {
            return matchRequestDto.getTeam2();
        }
    }

    private MatchRequestDto.Team getLosingTeam(MatchRequestDto matchRequestDto) {
        Integer team1Score = matchRequestDto.getTeam1().getScore();
        Integer team2Score = matchRequestDto.getTeam2().getScore();
        if (team1Score.compareTo(team2Score) < 0) {
            return matchRequestDto.getTeam1();
        } else {
            return matchRequestDto.getTeam2();
        }
    }
=======
//    public List<MatchResult> getMatchById(Long matchId) {
//        List<MatchResult> results = resultsRepository.findByMatchId(matchId);
//        Set<Long> playerIds = results.stream().map(r -> r.getPlayerId()).collect(Collectors.toSet());
//        playerApplicationService.getPlayersByIds(playerIds);
//        results.stream().map(r ->{
//
//        })
//
//    }
>>>>>>> Stashed changes
}
