package com.mages.magesbackend.infrastructure;

import org.springframework.stereotype.Component;
import com.mages.magesbackend.domain.PlayerResult;

@Component
public class ScoreComputingComponent {

    private static final Long D = 400l;
    private static final Integer K = 32;

    public Long getScoreChange(Long rank, Long oponentRank, PlayerResult result) {
        Double eloScore = computeEloScore(rank, oponentRank);
        Integer win = result == PlayerResult.WIN ? 1 : 0;
        Double scoreChange = rank + K * (win - eloScore);

        return scoreChange.longValue();
    }

    private Double computeEloScore(Long rank, Long oponentRank) {
        Double partialResult = Math.pow(10, (oponentRank - rank) / D);
        return 1 / (1 + partialResult);
    }
}
