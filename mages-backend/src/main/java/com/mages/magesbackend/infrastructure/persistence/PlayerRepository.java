package com.mages.magesbackend.infrastructure.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.mages.magesbackend.domain.Player;

import java.util.List;
import java.util.Set;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Long> {

    Player findById(Long id);

    List<Player> findByIdIn(Set<Long> playerIds);
}
