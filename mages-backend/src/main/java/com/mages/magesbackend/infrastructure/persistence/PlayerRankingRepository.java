package com.mages.magesbackend.infrastructure.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import com.mages.magesbackend.domain.PlayerRanking;

public interface PlayerRankingRepository extends JpaRepository<PlayerRanking, Long> {
}
