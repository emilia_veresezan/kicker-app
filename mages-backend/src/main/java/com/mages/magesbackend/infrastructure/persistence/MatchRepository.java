package com.mages.magesbackend.infrastructure.persistence;

import com.mages.magesbackend.domain.Match;
import com.mages.magesbackend.domain.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MatchRepository extends JpaRepository<Match, String> {

    Match findById(Long id);
}
