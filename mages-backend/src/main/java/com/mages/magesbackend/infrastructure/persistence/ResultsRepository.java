package com.mages.magesbackend.infrastructure.persistence;

import com.mages.magesbackend.domain.MatchResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResultsRepository extends JpaRepository<MatchResult, Long> {
    List<MatchResult> findByPlayerId(long playerId);
    List<MatchResult> findByMatchId(long matchId);
}
