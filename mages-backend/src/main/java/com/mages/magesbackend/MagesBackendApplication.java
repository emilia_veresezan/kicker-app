package com.mages.magesbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class MagesBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MagesBackendApplication.class, args);
	}
}
