package com.mages.magesbackend.domain;


import javax.persistence.*;


import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Table(name = "players")
@Entity
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String email;

    @Column
    private String pictureUrl;

    @OneToOne(cascade = CascadeType.ALL)
    private PlayerRanking playerRanking;

    public Player(String name, String email, String pictureUrl) {
        this.name = name;
        this.email = email;
        this.pictureUrl = pictureUrl;
        this.playerRanking = new PlayerRanking(PlayerRanking.STARTING_SCORE);
    }
}
