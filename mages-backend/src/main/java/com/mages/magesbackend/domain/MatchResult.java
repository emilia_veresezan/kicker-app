package com.mages.magesbackend.domain;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Table(name = "results")
@Entity
public class MatchResult {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long playerId;

    private PlayerResult playerResult;

    private Integer playerScore;

    private Long matchId;

    public MatchResult(Long playerId, PlayerResult playerResult, Integer playerScore, Long matchId) {
        this.playerId = playerId;
        this.playerResult = playerResult;
        this.playerScore = playerScore;
        this.matchId = matchId;
    }
}
