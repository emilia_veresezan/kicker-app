package com.mages.magesbackend.domain;

import javax.persistence.*;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
@Table(name = "player_ranking")
@Entity
public class PlayerRanking {

    public static Integer STARTING_SCORE = 1000;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer score;

    public PlayerRanking(Integer score) {
        this.score = score;
    }
}
