package com.mages.magesbackend.web.rest.dto;

import lombok.Data;

import java.util.List;

@Data
public class MatchRequestDto {
    private Team team1;
    private Team team2;

    public static class Team {
        private List<Long> playerIds;
        private Integer score;

        public List<Long> getPlayerIds() {
            return playerIds;
        }

        public Integer getScore() {
            return score;
        }
    }
}
