package com.mages.magesbackend.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class PlayerRankingDto {
    private Integer score;
}
