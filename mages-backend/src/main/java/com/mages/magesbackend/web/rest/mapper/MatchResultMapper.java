package com.mages.magesbackend.web.rest.mapper;

import com.mages.magesbackend.domain.MatchResult;
import com.mages.magesbackend.web.rest.dto.MatchResultDto;

public class MatchResultMapper {
    public static MatchResultDto mapMatchResultToMatchResultDto(MatchResult matchResult) {
    return new MatchResultDto(matchResult.getMatchId(), matchResult.getPlayerResult());
    }
}
