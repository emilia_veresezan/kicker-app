package com.mages.magesbackend.web.rest.dto;

import com.mages.magesbackend.domain.PlayerResult;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class MatchResultDto {
    private long matchId;

    private PlayerResult playerResult;

}
