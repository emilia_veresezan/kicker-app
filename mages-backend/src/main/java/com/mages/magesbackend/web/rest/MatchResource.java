package com.mages.magesbackend.web.rest;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.mages.magesbackend.application.MatchApplicationService;
import com.mages.magesbackend.web.rest.dto.MatchRequestDto;
import com.mages.magesbackend.web.rest.dto.MatchResultDto;
import com.mages.magesbackend.web.rest.mapper.MatchResultMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/matches")
public class MatchResource {

  @Autowired
  private MatchApplicationService matchApplicationsService;

  @RequestMapping(value = "", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<?> submitMatchResults(@RequestBody MatchRequestDto matchRequestDto) {

    matchApplicationsService.submitResults(matchRequestDto);

    return ResponseEntity.ok().build();
  }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<MatchResultDto>> getMatchesForPlayer(@RequestParam(name = "playerId") Long playerId) {
        List<MatchResultDto> matches = matchApplicationsService.getMatches(playerId).stream().map(MatchResultMapper::mapMatchResultToMatchResultDto).collect(Collectors.toList());

        return ResponseEntity.ok(matches);
    }

//  @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//  public ResponseEntity<List<MatchResultDto>> getMatchById(@RequestParam(name = "matchId") Long matchId) {
//    List<MatchResultDto> matches = matchApplicationsService.getMatchById(matchId).stream().map(MatchResultMapper::mapMatchResultToMatchResultDto).collect(Collectors.toList());
//
//    return ResponseEntity.ok(matches);
//  }
}
