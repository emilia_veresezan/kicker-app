package com.mages.magesbackend.web.rest;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import com.mages.magesbackend.application.PlayerApplicationService;
import com.mages.magesbackend.domain.Player;
import com.mages.magesbackend.web.rest.dto.PlayerDto;
import com.mages.magesbackend.web.rest.dto.PlayerRequestDto;
import com.mages.magesbackend.web.rest.mapper.PlayerMapper;


import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/players")
public class PlayerResource {

    @Autowired
    private PlayerApplicationService playerApplicationService;

    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<PlayerDto>> getAll() {
        List<PlayerDto> users = playerApplicationService.getPlayers().stream().map(PlayerMapper::mapUserToUserDto).collect(Collectors.toList());

        return ResponseEntity.ok(users);
    }

    @RequestMapping(value = "", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> createPlayer(@RequestBody PlayerRequestDto playerRequestDto) {
        Player player = PlayerMapper.mapRequestToUser(playerRequestDto);

        playerApplicationService.savePlayer(player);

        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/{playerId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<PlayerDto> getPlayer(@PathVariable(name = "playerId") Long playerId) {
        Player player = playerApplicationService.getPlayer(playerId);

        return ResponseEntity.ok(PlayerMapper.mapUserToUserDto(player));
    }

    @RequestMapping(value = "/{playerId}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> updateProfile(@RequestBody PlayerRequestDto playerRequestDto, @PathVariable(name = "playerId") Long playerId) {
        Player player = PlayerMapper.mapRequestToUser(playerRequestDto);
        player.setId(playerId);

        playerApplicationService.savePlayer(player);

        return ResponseEntity.ok().build();
    }
}
