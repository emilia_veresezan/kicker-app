package com.mages.magesbackend.web.rest.mapper;

import com.mages.magesbackend.domain.PlayerRanking;
import com.mages.magesbackend.web.rest.dto.PlayerRankingDto;

public class PlayerRankingMapper {
    public static PlayerRankingDto mapToDto(PlayerRanking playerRanking) {
        return new PlayerRankingDto(playerRanking.getScore());
    }

}
