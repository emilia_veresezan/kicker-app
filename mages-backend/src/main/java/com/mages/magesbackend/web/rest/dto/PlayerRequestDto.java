package com.mages.magesbackend.web.rest.dto;

import lombok.Getter;

@Getter
public class PlayerRequestDto {
    private String name;
    private String email;
    private String pictureUrl;
}
