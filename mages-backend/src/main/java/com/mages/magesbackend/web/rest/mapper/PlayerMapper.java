package com.mages.magesbackend.web.rest.mapper;

import com.mages.magesbackend.domain.Player;
import com.mages.magesbackend.web.rest.dto.PlayerDto;
import com.mages.magesbackend.web.rest.dto.PlayerRequestDto;

public class PlayerMapper {

    public static Player mapRequestToUser(PlayerRequestDto playerRequestDto) {
        return new Player(playerRequestDto.getName(), playerRequestDto.getEmail(), playerRequestDto.getPictureUrl());
    }

    public static PlayerDto mapUserToUserDto(Player player) {
        return new PlayerDto(player.getId(), player.getName(), player.getEmail(), player.getPictureUrl(), player.getPlayerRanking().getScore());
    }
}
