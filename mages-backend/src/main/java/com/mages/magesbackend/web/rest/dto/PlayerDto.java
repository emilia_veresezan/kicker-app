package com.mages.magesbackend.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class PlayerDto {
    private Long id;
    private String name;
    private String email;
    private String pictureUrl;

    private Integer playerScore;
}
